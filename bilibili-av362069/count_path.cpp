#include <iostream>
#include <memory>
#include <unordered_map>
#include <algorithm>

const int MAXN = 20;

typedef unsigned long long uint64;
typedef __uint128_t ans_type;

std::ostream& operator<< (std::ostream &s, __uint128_t n) {
	char c = '0' + (n % 10);
	n /= 10;
	if (n)
		s << n;
	return s << c;
}

inline int find_match(uint64 mask, int n, int pos) {
	static int q[MAXN], tail;
	tail = 0;
	for (int i = 0; i < n; i++) {
		int state = (mask >> (i << 1)) & 3;
		if (state == 1)
			q[tail++] = i;
		else if (state == 2) {
			tail--;
			if (i == pos)
				return q[tail];
			else if (q[tail] == pos)
				return i;
		}
	}
	return -1;
}


ans_type count_path(int n) {
	std::unique_ptr<std::unordered_map<uint64, ans_type>> hash_map(new std::unordered_map<uint64, ans_type>()), tmp_map(new std::unordered_map<uint64, ans_type>());
	(*hash_map)[3] = 1;
	(*hash_map)[3 << 2] = 1;
	ans_type ans = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i == 0 && j == 0) continue;

			for (std::pair<uint64, ans_type> p : *hash_map) {
				if (j == 0)
					p.first <<= 2;
				uint64 left = (p.first >> (j << 1)) & 3, above = (p.first >> ((j + 1) << 1)) & 3;
				if (left == 0 && above == 0) {
					(*tmp_map)[p.first] += p.second;
					if (i < n - 1 && j < n - 1) {
						(*tmp_map)[p.first ^ (1 << (j << 1)) ^ (2 << ((j + 1) << 1))] += p.second;
					}
				} else if ((left == 0 && above > 0) || (left > 0 && above == 0)) {
					uint64 state = left + above;
					uint64 clear_mask = p.first ^ (left << (j << 1)) ^ (above << ((j + 1) << 1));
					if (j < n - 1) {
						(*tmp_map)[clear_mask ^ (state << ((j + 1) << 1))] += p.second;
					}
					if (i < n - 1) {
						(*tmp_map)[clear_mask ^ (state << (j << 1))] += p.second;
					}
					if (i == n - 1 && j == n - 1 && state == 3) {
						ans += p.second;
					}
				} else {
					uint64 clear_mask = p.first ^ (left << (j << 1)) ^ (above << ((j + 1) << 1));
					if (left == 1 && above == 2) continue;
					if (left == 2 && above == 1) {
						(*tmp_map)[clear_mask] += p.second;
					} else {
						if (left == 3 && above == 1) {
							int match_above = find_match(p.first, n + 1, j + 1);
							(*tmp_map)[clear_mask ^ (2 << (match_above << 1)) ^ (3 << (match_above << 1))] += p.second;
						} else if (above == 3 && left == 2) {
							int match_left = find_match(p.first, n + 1, j);
							(*tmp_map)[clear_mask ^ (1 << (match_left << 1)) ^ (3 << (match_left << 1))] += p.second;							
						} else if (left == 1 && above == 1) {
							int match_above = find_match(p.first, n + 1, j + 1);
							(*tmp_map)[clear_mask ^ (2 << (match_above << 1) ^ (1 << (match_above << 1)))] += p.second;
						} else if (left == 2 && above == 2) {
							int match_left = find_match(p.first, n + 1, j);
							(*tmp_map)[clear_mask ^ (1 << (match_left << 1)) ^ (2 << (match_left << 1))] += p.second;
						}
					}
				}
			}

			std::swap(hash_map, tmp_map);
			tmp_map->clear();
		}
	}
	return ans;
}

int main() {
	int n;
	std::cin >> n;
	std::cout << count_path(n + 1) << std::endl;
}